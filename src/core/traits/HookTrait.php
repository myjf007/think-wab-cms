<?php
/**
 * Created by PhpStorm.
 * @User: abo
 * @author: wab <115217614@qq.com>
 * @Date: 2018/6/24
 * @Time: 21:34
 */

namespace wab\core\traits;


trait HookTrait
{
    /**
     * 默认钩子方法
     * @var string
     */
    protected $_hookMethod = 'hook';

    /**
     * 钩子
     * @var array
     */
    protected $_hooks = [];

    /**
     * @todo: 获取钩子
     * @param null $name
     * @author: wab <115217614@qq.com>
     * @return array
     */
    public function getHook($name  = null)
    {
        if (empty($name)){
            return $this->_hooks;
        }else{
            return isset($this->_hooks[$name]) ? $this->_hooks[$name] : [];
        }
    }

    /**
     * @todo: 增加钩子
     * @param string $name     钩子名
     * @param mixed $behavior  行为
     * @param int $weight      权重
     * @param string $key      标识
     * @author: wab <115217614@qq.com>
     */
    public function addHook($name, $behavior, $weight = 0, $key = null)
    {
        if (is_array($behavior)){
            /**
             * 添加多个钩子
             */
            foreach ($behavior as $item) {
                $this->addHook($name, $item, $key, $weight);
            }
        }else{
            isset($this->_hooks['name']) || $this->_hooks[$name] = [];

            /**
             * 避免重复添加
             */
            if (is_null($key)){
                if ($behavior instanceof \Closure){
                    $key = md5(time() . rand(1, 100000));
                }else{
                    $key = md5(serialize($behavior));
                }
            }
            $this->_hooks[$name][$key] = $this->formatHook($behavior, $weight, $key);
        }
    }

    /**
     * @todo: 调用钩子
     * @param string $name  钩子名
     * @param mixed $params 参数
     * @param mixed $extra  额外参数
     * @author: wab <115217614@qq.com>
     * @return array
     */
    public function callHook($name, &$params, $extra = null)
    {
        $results = [];
        $hooks = $this->getHook($name);
        // 排序
        usort($hooks, [
            $this,
            'compareHookWeight'
        ]);
        foreach ($hooks as $key => $hook) {
            $results[$key] = $this->executeHook($hook['behavior'], $params, $extra);
            // 返回false则停止执行
            if (false === $results[$key]) {
                break;
            }
        }
        return $results;
    }

    /**
     * @todo: 重置钩子
     * @author: wab <115217614@qq.com>
     */
    public function resetHook()
    {
        $this->_hooks = [];
    }

    /**
     * @todo: 执行钩子
     * @param string $behavior 行为
     * @param mixed $params    参数
     * @param mixed $extra     额外参数
     * @author: wab <115217614@qq.com>
     * @return mixed
     */
    protected function executeHook($behavior, &$params = null, $extra = null)
    {
        if (is_string($behavior)) {
            if (strpos($behavior, '::')) {
                $behavior = explode('::', $behavior, 2);
            } else {
                $behavior = [
                    $behavior,
                    $this->_hookMethod
                ];
            }
        }
        return call_user_func_array($behavior, [
            &$params,
            $extra,
            $this
        ]);
    }

    /**
     * @todo: 比较两个钩子权值
     * @param array $a 钩子1
     * @param array $b 钩子2
     * @author: wab <115217614@qq.com>
     * @return bool
     */
    protected function compareHookWeight($a, $b)
    {
        return $a['weight'] < $b['weight'];
    }

    /**
     * @todo: 格式化钩子
     * @param string $behavior 行为
     * @param int $weight      权重
     * @param string $key      标识
     * @author: wab <115217614@qq.com>
     * @return array
     */
    protected function formatHook($behavior, $weight = 0, $key = null)
    {
        return [
            'behavior' => $behavior,
            'weight'   => $weight,
            'key'      => $key
        ];
    }
}