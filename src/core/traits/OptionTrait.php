<?php
/**
 * Created by PhpStorm.
 * @User: abo
 * @author: wab <115217614@qq.com>
 * @Date: 2018/6/24
 * @Time: 22:02
 */

namespace wab\core\traits;


trait OptionTrait
{

    /**
     * 选项
     * @var array
     */
    private $_option = [];

    /**
     * @todo: 构造函数
     * @author: wab <115217614@qq.com>
     * OptionTrait constructor.
     * @param array $option
     */
    public function __construct($option = [])
    {
        $this->_option = $option;
    }

    /**
     * @todo: 获取选项
     * @param string $name  选项名
     * @param mixed $option 选项值
     * @author: wab <115217614@qq.com>
     * @return array|null
     */
    public function getOption($name = null, $option = null)
    {
        if (is_null($name)) {
            return $this->_option;
        } else {
            $option || $option = $this->getOption();
            if (strpos($name, '.') !== false) {
                list ($key, $name) = explode('.', $name, 2);
                return isset($option[$key]) && is_array($option[$key]) ? $this->getOption($name, $option[$key]) : null;
            } else {
                return isset($option[$name]) ? $option[$name] : null;
            }
        }
    }

    /**
     * @todo: 设置选项
     * @param mixed $name  选项名
     * @param mixed $value 选项值
     * @author: wab <115217614@qq.com>
     */
    public function setOption($name, $value = null)
    {
        if (is_array($name)) {
            $this->_option = $name;
        } elseif (is_null($value)) {
            unset($this->_option[$name]);
        } else {
            $this->_option[$name] = $value;
        }
    }

}