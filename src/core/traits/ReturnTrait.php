<?php
/**
 * Created by PhpStorm.
 * @User: abo
 * @author: wab <115217614@qq.com>
 * @Date: 2018/6/24
 * @Time: 21:17
 */

namespace wab\core\traits;


use wab\core\constant\CodeConstant;
use wab\core\objects\ReturnObject;

trait ReturnTrait
{
    /**
     * 消息映射
     * @var array
     */
    protected $_msgMapping = [
        CodeConstant::CODE_SUCCESS => 'success',
        CodeConstant::CODE_ERROR   => 'error'
    ];

    /**
     * @todo: 消息映射
     * @author: wab <115217614@qq.com>
     * @return array
     */
    public function getMsgMapping()
    {
        return [];
    }

    /**
     * @todo: 返回成功提示
     * @param string $msg  提示消息
     * @param mixed $data 返回的数据
     * @author: wab <115217614@qq.com>
     * @return ReturnObject
     */
    protected function returnSuccess($msg = '', $data  ='')
    {
        return $this->returnData(CodeConstant::CODE_SUCCESS, $msg, $data);
    }

    /**
     * @todo: 返回失败提示
     * @param string $msg  提示消息
     * @param mixed $data 返回的数据
     * @author: wab <115217614@qq.com>
     * @return ReturnObject
     */
    protected function returnError($msg = '', $data = '')
    {
        return $this->returnData(CodeConstant::CODE_ERROR, $msg, $data);
    }

    /**
     * @todo: 返回数据
     * @param int $code    状态码
     * @param string $msg  提示消息
     * @param mixed $data 返回的数据
     * @author: wab <115217614@qq.com>
     * @return ReturnObject
     */
    protected function returnData($code, $msg = '', $data = '')
    {
        $object = new ReturnObject();

        /**
         * 设置状态码
         */
        $object->setCode($code);

        /**
         * 设置提示消息
         */
        $msgMapping = array_merge($this->_msgMapping, $this->getMsgMapping());
        if (empty($msg) && isset($msgMapping[$code])){
            $msg = $msgMapping[$code];
        }
        $object->setMsg($msg);

        /**
         * 设置数据
         */
        $object->setData($data);

        return $object;
    }
}