<?php
/**
 * Created by PhpStorm.
 * @User: abo
 * @author: wab <115217614@qq.com>
 * @Date: 2018/6/24
 * @Time: 21:29
 */

namespace wab\core\traits;


trait InstanceTrait
{
    /**
     * @todo: 获取实例
     * @author: wab <115217614@qq.com>
     * @return $this
     */
    public static function getInstance()
    {
        return new static();
    }

    /**
     * @todo: 获取单实例
     * @author: wab <115217614@qq.com>
     * @return $this
     */
    public static function getSingleon()
    {
        static $instance = null;

        if (!isset($instance)){
            $instance = self::getInstance();
        }

        return $instance;
    }
}