<?php
/**
 * Created by PhpStorm.
 * @User: abo
 * @author: wab <115217614@qq.com>
 * @Date: 2018/6/24
 * @Time: 22:04
 */

namespace wab\core\support;


use think\facade\Env;
use think\facade\Request;
use think\Loader;
use wab\core\traits\InstanceTrait;

class Util
{
    /**
     * 实例Trait
     */
    use InstanceTrait;

    /**
     * @todo: 获取客户端IP
     * @author: wab <115217614@qq.com>
     * @return mixed
     */
    public function getIp()
    {
        return Request::ip();
    }

    /**
     * @todo: 获取客户端浏览器标识
     * @author: wab <115217614@qq.com>
     * @return mixed
     */
    public function getAgent()
    {
        return Request::server('HTTP_USER_AGENT');
    }

    /**
     * @todo: 获取当前操作
     * @author: wab <115217614@qq.com>
     * @return string
     */
    public function getCurrentAction()
    {
        return Request::module() . '/' . Loader::parseName(Request::controller()) . '/' . Request::action();
    }

    /**
     * @todo: 随机哈希字符串
     * @param int $length
     * @param string $prefix
     * @author: wab <115217614@qq.com>
     * @return string
     */
    public function randHashStr($length = 16, $prefix = '')
    {
        $size = $length - strlen($prefix);
        $offset = intval(32 - $size) / 2;
        return $prefix . substr(md5(microtime(true) . mt_rand(1000, 9999)), $offset, $size);
    }

    /**
     * @todo: 临时文件
     * @param null $prefix
     * @author: wab <115217614@qq.com>
     * @return bool|string
     */
    public function tmpFile($prefix = null)
    {
        $tmpPath = Env::get('RUNTIME_PATH') . 'file/';
        // 不存在则创建
        is_dir($tmpPath) || mkdir($tmpPath, 0755, true);
        $prefix || $prefix = 'tmp';
        return tempnam($tmpPath, $prefix);
    }

    /**
     * @todo: 读取文件
     * @param $filePath
     * @author: wab <115217614@qq.com>
     * @return bool|string
     */
    public function readFile($filePath)
    {
        try {
            $file = fopen($filePath, 'r');
            $content = fread($file, filesize($filePath));
            fclose($file);
            return $content;
        } catch (\Exception $e) {
            return '';
        }
    }

    /**
     * @todo: 转换文件大小
     * @param string $size 文件大小
     * @author: wab <115217614@qq.com>
     * @return float|int
     */
    public function translateBytes($size)
    {
        $units = [
            'k' => 1,
            'm' => 2,
            'g' => 3,
            't' => 4,
            'p' => 5
        ];
        $size = strtolower($size);
        $bytes = intval($size);
        foreach ($units as $key => $value) {
            if (strpos($size, $key)) {
                $bytes = $bytes * pow(1024, $value);
                break;
            }
        }
        return $bytes;
    }

    /**
     * @todo: 文件大小格式化
     * @param $size
     * @param string $delimiter
     * @author: wab <115217614@qq.com>
     * @return string
     */
    public function formatBytes($size, $delimiter = '')
    {
        $units = [
            'B',
            'KB',
            'MB',
            'GB',
            'TB',
            'PB'
        ];
        for ($i = 0; $size >= 1024 && $i < 5; $i++) {
            $size /= 1024;
        }
        return round($size, 2) . $delimiter . $units[$i];
    }
}