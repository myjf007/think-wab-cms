<?php
/**
 * Created by PhpStorm.
 * @User: abo
 * @author: wab <115217614@qq.com>
 * @Date: 2018/6/24
 * @Time: 21:07
 */

namespace wab\core\constant;


class CodeConstant
{
    /**
     * 成功
     */
    const CODE_SUCCESS = 200;

    /**
     * 失败
     */
    const CODE_ERROR = 400;

    
}