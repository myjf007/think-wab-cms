<?php
/**
 * Created by PhpStorm.
 * @User: abo
 * @author: wab <115217614@qq.com>
 * @Date: 2018/6/24
 * @Time: 21:08
 */

namespace wab\core\objects;


use wab\core\constant\CodeConstant;

class ReturnObject
{
    /**
     * 状态码
     * @var int
     */
    protected $code = CodeConstant::CODE_SUCCESS;

    /**
     * 提示
     * @var string
     */
    protected $msg = '';

    /**
     * 数据
     * @var mixed
     */
    protected $data = '';

    /**
     * @todo: 是否成功
     * @author: wab <115217614@qq.com>
     * @return bool
     */
    public function isSuccess()
    {
        return $this->getCode() ==  CodeConstant::CODE_SUCCESS;
    }

    /**
     * @todo: 获取状态码
     * @author: wab <115217614@qq.com>
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @todo: 设置状态码
     * @param int $code 状态码
     * @author: wab <115217614@qq.com>
     */
    public function setCode($code)
    {
        $this->code =  $code;
    }

    /**
     * @todo: 获取消息
     * @author: wab <115217614@qq.com>
     * @return string
     */
    public function getMsg()
    {
        return $this->msg;
    }

    /**
     * @todo: 设置消息
     * @author: wab <115217614@qq.com>
     * @param string $msg
     */
    public function setMsg($msg)
    {
        $this->msg = $msg;
    }

    /**
     * @todo: 获取数据
     * @author: wab <115217614@qq.com>
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @todo: 设置数据
     * @author: wab <115217614@qq.com>
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @todo: 获取返回的响应
     * @author: wab <115217614@qq.com>
     * @return array
     */
    public function getResponse()
    {
        return [
            'code' => $this->getCode(),
            'msg'  => $this->getMsg(),
            'data' => $this->getData()
        ];
    }


}