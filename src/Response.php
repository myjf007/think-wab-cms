<?php
/**
 * Created by PhpStorm.
 * @User: abo
 * @author: wab <115217614@qq.com>
 * @Date: 2018/6/24
 * @Time: 20:56
 */

namespace wab;



use think\exception\HttpResponseException;
use think\facade\Url;
use wab\core\traits\InstanceTrait;

class Response
{
    /**
     * 实例Trait
     */
    use InstanceTrait;

    /**
     * @todo: API返回
     * @param int $code      状态码
     * @param string $msg    提示消息
     * @param string $data   要返回的数据
     * @param array $header  响应头信息
     * @param array $options 选项
     * @author: wab <115217614@qq.com>
     */
    public function api($code, $msg = '', $data = '', $header = [], $options = [])
    {
        $this->json([
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        ], $header, $options);
    }

    /**
     * @todo: 返回跳转
     * @param $url
     * @param bool $build
     * @param array $header
     * @param array $options
     * @author: wab <115217614@qq.com>
     */
    public function redirect($url, $build = true, $header = [], $options = [])
    {
        $header['Location'] = $build ? $this->buildUrl($url) : $url;
        $this->data('', '', 302, $header, $options);
    }

    /**
     * @todo: 返回Xml
     * @param string $xml
     * @param array $header
     * @param array $options
     * @author: wab <115217614@qq.com>
     */
    public function xml($xml, $header = [], $options = [])
    {
        $this->data($xml, 'xml', 200, $header, $options);
    }

    /**
     * @todo: 返回Json
     * @param array $json    要返回的Json数据
     * @param array $header  响应头信息
     * @param array $options 选项
     * @author: wab <115217614@qq.com>
     */
    public function json($json, $header = [], $options = [])
    {
        $this->data($json, 'json', 200, $header, $options);
    }

    /**
     * @todo: 返回结果
     * @param mixed  $data   要返回的数据
     * @param string $type   返回的数据类型
     * @param int $code      状态码
     * @param array $header  响应头信息
     * @param array $options 选项
     * @author: wab <115217614@qq.com>
     */
    public function data($data, $type = 'auto', $code = 200, $header = [], $options = [])
    {
        $type == 'auto' && $type = is_array($data) ? 'json' : '';
        $response = \think\Response::create($data, $type, $code, $header, $options);
        throw new HttpResponseException($response);
    }

    /**
     * @todo: 构造url
     * @param string $url 链接地址
     * @author: wab <115217614@qq.com>
     * @return mixed
     */
    protected function buildUrl($url)
    {
        if (strpos($url, '://') || 0 === strpos($url, '/')) {
            return $url;
        } elseif ($url === '') {
            return $url;
        } else {
            return Url::build($url);
        }
    }
}