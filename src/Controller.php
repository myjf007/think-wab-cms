<?php
/**
 * Created by PhpStorm.
 * @User: abo
 * @author: wab <115217614@qq.com>
 * @Date: 2018/6/24
 * @Time: 20:55
 */

namespace wab;


use think\facade\Config;
use think\facade\Request;
use think\facade\View;

class Controller
{
    /**
     * @var \think\View
     */
    protected $view;

    /**
     * @var \think\Request
     */
    protected $request;

    public function __construct()
    {
        $this->view = View::init(Config::pull('template'));
        $this->request =  Request::instance();

        // 初始化
        $this->_initialize();

    }

    /**
     * @todo: 初始化
     * @author: wab <115217614@qq.com>
     */
    protected function _initialize()
    {

    }

    /**
     * @todo: 视图渲染前
     * @author: wab <115217614@qq.com>
     */
    protected function beforeViewRender()
    {

    }

    protected function success($msg = '', $url = '', $data = '', $wait = 3, $header = [])
    {

    }

    protected function error($msg = '', $url = '', $data = '', $wait = 3, $header = [])
    {

    }

    protected function jump()
    {

    }

    protected function response($return, $url = '', $wait = 3, $header = [])
    {

    }

    protected function assign()
    {

    }

    protected function fetch()
    {

    }

    protected function display()
    {

    }

    protected function buildUrl($url)
    {

    }

    protected function getJumpTemplate($code)
    {

    }
}